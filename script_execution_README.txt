Step 1. IF using interactive node
	sinteractive --gres=lscratch:100 --time=01-00:00:00 --mem=60g

Step 2. Please perform one of the following to setup conda environment for executing LOHHLA tool.
a.Please download and set conda in your workspace. And create a new conda environment using the "LOHHLA.yml". (file provided in the repository.)
 conda env create -f LOHHLA.yml 
b.Activate the exsisting LOHHLA environment.
  source <path>/conda/etc/profile.d/conda.sh
  conda activate LOHHLA

Step 3. Prepare predicted HLA file. For example "<path>/hlas_class_I_II". (file provided in the repository.)
Step 4.	Prepare copynumber file. For example "<path>/copynumber.txt". (file provided in the repository.)
Step 5. Edit 'example_class_I_II' script. (file provided in the repository.) 

Please follow the below steps 

	## Make changes to 'example_class_I_II.sh' script
	1) Change the desired script to execute "LOHHLAscript.R"
	2) --patientId
	3) --outputDir
	4) --normalBAMfile
	5) --BAMDir : All the *.bams (library bams) *.bai (bam index) that need to be checked for LOH and should be associated with above normal bam file
	6) --hlaPath : file with predicted HLAs of the patient
	7) --CopyNumLoc: File with copy number details of each library that need to be tested
	8) --coordinateFile: File with coordinates of HLA classI & classII allele loci and alternate scaffold names.
Step 6. Legend for plots generated at the end

Following plots are repeated for each MHC class I & II gene
Page 1. Tumor/Normal coverage for each allele.
Page 2. Overlap plot showing Log-Ratios of SNPs for each allele.
Page 3. Read coverage of  both alleles in Normal (PBL)
Page 4 &5. Read coverage of each allele in tumor & normal
Page 6. Log ratio plot (needs additional documentation)
Page 7. Copy Number for each allele using Copy number derived using Log R ratio (LRR) + B allele frequency (BAF) + tumor purity + tumor ploidy.
Page 8. Copy Number for each allele using Copy number derived using Log R ratio (LRR) + tumor purity + tumor ploidy.
Page 9. Statistical test ( t.test ) of logR ratio of matched SNPs between two alleles.
Page 10. Statistical test ( t.test ) of logR ratio of mismatched SNPs between two alleles
Page 11. Correlation plot of read coverage in tumor vs normal for each allele.
Page 12. Rolling mean log tumor/normal
Page 13. Log Ratio plot (needs additional documentation)
Page 14. Statistical test ( t.test ) of logR ratio of SNPs between exactly matching reads and mismatching reads.
