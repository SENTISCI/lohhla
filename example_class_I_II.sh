#!/bin/bash

## Platform: Linux
##Source your conda
source <path>/conda/etc/profile.d/conda.sh
## Activate LOHHLA conda environment . You can make one using the .yml provided in the repository.
conda activate LOHHLA
## Load all the required modules
module load bedtools/2.25.0
module load samtools/1.3.1
module load novocraft
module load jellyfish/2.2.7
## Script to execute
Rscript <path>/LOHHLAscript.R --patientId Exome --outputDir <path>/output_classII_04152020 --normalBAMfile <path>/PBL.bam --BAMDir <path>/bams_tumor_dir --hlaPath <path>/hlas_class_I_II --coordinateFile <path>/GRCh37_MHC_coordinates_AS.txt --HLAfastaLoc <path>/hla_ref/hla_gen.fasta --HLAexonLoc <path>/hla_ref/hla.dat --CopyNumLoc copynumber.txt --mappingStep TRUE --coverageStep TRUE --minCoverageFilter 10 --fishingStep TRUE --cleanUp FALSE --gatkDir <path>/gatk --novoDir <path>novocraft/4.02.02/
